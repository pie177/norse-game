﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StaminaBar : MonoBehaviour {
    public Image staminaBar;
    [Tooltip("for amounts from 0 to 1 unless you divide it in code.")]
    public float maxStamina;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        staminaBar.fillAmount = maxStamina;
	}
}
