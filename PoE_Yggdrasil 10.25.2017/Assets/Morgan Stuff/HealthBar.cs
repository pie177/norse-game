﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public Image healthBar;
    [Tooltip("for amounts from 0 to 1 unless you divide it in code.")]
    public float maxHealth;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        healthBar.fillAmount = maxHealth;
	}
}
