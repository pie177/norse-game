﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SyncRoatation_Player : NetworkBehaviour {

    [SyncVar] Quaternion syncPlayerRotation;
    [SyncVar] Quaternion syncCamRotation;

    [SerializeField] Transform playerTransform;
    [SerializeField] Transform camTransform;
    [SerializeField] float lerpRate = 15f;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        TransmitRotations();
        LerpRotations();
	}

    void LerpRotations()
    {
        if (!isLocalPlayer)
        {
            playerTransform.rotation = Quaternion.Lerp(playerTransform.rotation, syncPlayerRotation, Time.deltaTime * lerpRate);
            camTransform.rotation = Quaternion.Lerp(camTransform.rotation, syncCamRotation, Time.deltaTime * lerpRate);
        }
    }

    [Command]
    void CmdGiveServerRotation(Quaternion playerRot, Quaternion camRot)
    {
        syncPlayerRotation = playerRot;
        syncCamRotation = camRot;
    }

    [Client]
    void TransmitRotations()
    {
        if(isLocalPlayer)
        {
            CmdGiveServerRotation(playerTransform.rotation, camTransform.rotation);
        }
    }
}
