﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Monster_Controller : NetworkBehaviour {

    public float movementSpeed;
    public float jumpSpeed;
    public float rotateSpeed;
    public Camera camera;

    private CharacterController charController;
    private Vector3 moveDirection = Vector3.zero;
    private float gravity = 9.8f;

    // Use this for initialization
    void Start ()
    {
        if (isLocalPlayer)
        {
            charController = GetComponent<CharacterController>();
            //LockCursor();
        }

        else
        {
            Destroy(gameObject.GetComponentInChildren<Camera>(), 0f);
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (!isLocalPlayer)
        {
            return;
        }
        if (charController.isGrounded == true)
        {
            moveDirection = new Vector3(0, 0, Input.GetAxis("Vertical"));

            moveDirection = transform.TransformDirection(moveDirection);

            moveDirection *= movementSpeed;

            if (Input.GetButton("Jump"))
            {
                moveDirection.y += jumpSpeed;
            }
        }
        moveDirection.y -= gravity * Time.deltaTime;

        charController.Move(moveDirection * Time.deltaTime);

        if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(new Vector3(0, -1 * rotateSpeed * Time.deltaTime, 0));
        }

        if(Input.GetKey(KeyCode.D))
        {
            transform.Rotate(new Vector3(0, rotateSpeed * Time.deltaTime, 0));
        }
    }
}
