﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class camMouse : MonoBehaviour {


    public float sensitivity = 100f;

	// Use this for initialization
	void Start ()
    {
        
	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.Rotate(new Vector3(-Input.GetAxis("Mouse Y"), 0, 0) * sensitivity * Time.deltaTime);
    }
}
