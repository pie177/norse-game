﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class ScriptShooting : NetworkBehaviour {

  //public float bulletLife = 200f;
  public Transform bulletSpawnPoint;
  public float power = 800f;
  public GameObject bullet;
    // Morgan's Stuff
  public Text DecAmmo;
  public Text DecMag;
  public int AmmoCount = 100;
  public int MagCounter = 1000;


	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
    if(!isLocalPlayer) { return; }

    if(Input.GetButtonDown("Fire1")) {
      CmdFireBullet();
    }
    }
    //Morgan's Stuff
    public void DecreaseAmmo()
    {
        if (AmmoCount >= 0)
        {
            ReloadGun();
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            ReloadGun();
            MagCounter = MagCounter - (100 - AmmoCount);
            DecMag.text = MagCounter.ToString();
        }
    }
    

    public IEnumerator ReloadGun()
    {
        yield return new WaitForSeconds(5);
    }

  //void FixedUpdate(){
  //  if(bulletLife < 0) {
  //    bulletLife--;
  //  }
  //  else
  //    CmdDestoryBullet();
  //}

  [Command]
  void CmdFireBullet() {
    GameObject tempBullet = Instantiate(bullet, bulletSpawnPoint.position, bulletSpawnPoint.rotation) as GameObject;

    tempBullet.GetComponent<Rigidbody>().AddForce(bulletSpawnPoint.forward * power);

    NetworkServer.Spawn(tempBullet);
  }

  //[Command]
  //void CmdDestoryBullet() {
  //  Destory(bullet);
  //}
}
