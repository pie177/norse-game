﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Health : NetworkBehaviour
{

    public const int maxHealth = 100;
    [SyncVar] int currentHealth = maxHealth;

    private NetworkStartPosition[] spawnPoints;
    List<NetworkStartPosition> playerSpawns = new List<NetworkStartPosition>();

    private void Start()
    {
        if(isLocalPlayer)
        {
            spawnPoints = FindObjectsOfType<NetworkStartPosition>();
            foreach(NetworkStartPosition point in spawnPoints)
            {
                if(point.CompareTag("HunterSpawn"))
                {
                    playerSpawns.Add(point);
                }
            }

            RpcRespawn();
        }
    }

    public void TakeDamage(int amount)
    {
        if (currentHealth <= 0)
        {
            currentHealth = 0;
            RpcRespawn();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Support"))
        {
            currentHealth += 20;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.CompareTag("Support"))
        {
            currentHealth -= 20;
        }
    }

    [ClientRpc]
    void RpcRespawn()
    {
        if(isLocalPlayer)
        {
            Vector3 respawnPoint = Vector3.zero;

            if(playerSpawns != null && playerSpawns.Count> 0)
            {
                respawnPoint = playerSpawns[Random.Range(0, spawnPoints.Length)].transform.position;
            }

            transform.position = respawnPoint;
        }
    }
}
