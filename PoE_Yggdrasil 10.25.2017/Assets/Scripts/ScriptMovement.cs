﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ScriptMovement : NetworkBehaviour {

  public float moveSpeed = 4f;

  // Use this for initialization
  void Start() {

  }

  // Update is called once per frame
  void Update() {
    if (!isLocalPlayer) { return; }

    if (Input.GetKey(KeyCode.A)) {
      gameObject.transform.Translate(Vector3.left * moveSpeed * Time.deltaTime);
    }
    if (Input.GetKey(KeyCode.S)) {
      gameObject.transform.Translate(Vector3.back * moveSpeed * Time.deltaTime);
    }
    if (Input.GetKey(KeyCode.W)) {
      gameObject.transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);
    }
    if (Input.GetKey(KeyCode.D)) {
      gameObject.transform.Translate(Vector3.right * moveSpeed * Time.deltaTime);
    }

    float mHorizon = Input.GetAxis("Mouse X");
    float mVert = Input.GetAxis("Mouse Y");

    transform.Rotate(mVert,-mHorizon, 0);
  }
}
