﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class SceneChange : MonoBehaviour {

    // Use this for initialization
    void Start()
    {

    }
	public void OnGameStart()
    {
        Debug.Log("You Pressed Start Game!");

        //Load Nex Scene
        SceneManager.LoadScene("SceneOne");
        SceneManager.LoadScene("MainMenu");
    }

    public void OptionsScene()
    {
        Debug.Log("Options Loaded");

        SceneManager.LoadScene("Options");
    }

    public void CreditsScene()
    {
        Debug.Log("Credits Loaded");

        SceneManager.LoadScene("Credits");
    }

    public void CharacterSelect()
    {
        Debug.Log("Characters Loaded");

        SceneManager.LoadScene("Lobby");
    }

    public void MonsterSelect()
    {
        Debug.Log("Monsters Loaded");

        SceneManager.LoadScene("MonsterSelect");
    }

    public void MatchMakkingScene()
    {
        Debug.Log("Entering MatchMaking");

        SceneManager.LoadScene("MatchMakingScene");
    }

    public void QuitGame()
    {
        Debug.Log("You Quit the Game");

        Application.Quit();
    }


	// Update is called once per frame
	void Update () {

		
	}
}
