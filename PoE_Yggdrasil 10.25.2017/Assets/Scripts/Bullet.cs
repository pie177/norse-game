﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public int playerDamage = 10;
    public int monsterDamage = 20;

    private void Start()
    {
        Destroy(gameObject, 3f);
    }

    private void OnCollisionEnter(Collision collision)
    {
        GameObject hit = collision.gameObject;

        if(hit.CompareTag("Player") || hit.CompareTag("MachineGunner"))
        {
            Health health = hit.GetComponent<Health>();

            health.TakeDamage(playerDamage);

            Destroy(gameObject);
        }

        if(hit.CompareTag("Monster"))
        {
            Health health = hit.GetComponent<Health>();

            health.TakeDamage(monsterDamage);

            Destroy(gameObject);
        }

    }
}