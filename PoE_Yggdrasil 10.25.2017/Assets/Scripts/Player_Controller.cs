﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Player_Controller : NetworkBehaviour
{

    public GameObject bullet;
    public Transform bulletSpawn;
    public GameObject resupply;
    public float movementSpeed;
    public float jumpSpeed;
    public float shootForce;
    public GameObject camera;
    public float rotationSpeed;

    //UI Variables
    //public Text DecAmmo;
    //public Text DecMag;
    public int AmmoCount = 100;
    public int MagCounter = 1000;


    private CharacterController charController;
    private Vector3 moveDirection = Vector3.zero;
    private float gravity = 9.8f;

    // Use this for initialization
    void Start()
    {
        if (isLocalPlayer)
        {
            charController = GetComponent<CharacterController>();
            LockCursor();
        }

        else if (!isLocalPlayer || !isServer)
        {
            camera.GetComponent<Camera>().enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!isLocalPlayer)
        {
            return;
        }
        if (charController.isGrounded == true)
        {
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

            moveDirection = transform.TransformDirection(moveDirection);

            moveDirection *= movementSpeed;

            if (Input.GetButton("Jump"))
            {
                moveDirection.y += jumpSpeed;
            }
        }
        moveDirection.y -= gravity * Time.deltaTime;

        charController.Move(moveDirection * Time.deltaTime);

        if (Input.GetMouseButton(0) && this.gameObject.CompareTag("MachineGunner"))
        {
            //check for ammo
            CmdShoot();
            //AmmoCount--;
            //MagCounter = MagCounter - (100 - AmmoCount);
            //DecMag.text = MagCounter.ToString();
        }

        else if (Input.GetMouseButtonDown(0))
        {
            //check for ammo
            CmdShoot();
            //AmmoCount--;
            //MagCounter = MagCounter - (100 - AmmoCount);
            //DecMag.text = MagCounter.ToString();
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            LockCursor();
        }

        transform.Rotate(new Vector3(0, Input.GetAxis("Mouse X"), 0) * rotationSpeed * Time.deltaTime);
    }

    public override void OnStartLocalPlayer()
    {
        GetComponent<MeshRenderer>().material.color = Color.blue;
    }

    [Command]
    void CmdShoot()
    {
        Vector3 bulletspawning = new Vector3(bulletSpawn.position.x, camera.transform.position.y, bulletSpawn.position.z);

        //creat a ray that goes from camera position in the direction the camera is facing
        Ray rayOrigin = new Ray(bulletspawning, camera.transform.forward);

        GameObject tempBullet = (GameObject)Instantiate(bullet, rayOrigin.origin, bulletSpawn.localRotation);

        // tell it to ignore player collider
        //Physics.IgnoreCollision(tempBullet.GetComponent<Collider>(), GetComponent<CharacterController>());

        //tempBullet.GetComponent<Rigidbody>().AddRelativeForce(rayOrigin.direction * shootForce, ForceMode.Impulse);

        tempBullet.GetComponent<Rigidbody>().velocity = rayOrigin.direction * shootForce;

        NetworkServer.Spawn(tempBullet);

        Destroy(tempBullet, 3f);
    }

    [Command]
    void CmdDropAmmo()
    {

    }

    void LockCursor()
    {
        //is cursor locked
        if (Cursor.lockState == CursorLockMode.Locked)
        {
            //set it to not be locked
            Cursor.lockState = CursorLockMode.None;
        }
        else
        {
            //set it to locked
            Cursor.lockState = CursorLockMode.Locked;
        }
        //toggle cursor visibility
        Cursor.visible = !Cursor.visible;
    }
}